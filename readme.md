# FourierSettingsFitting

A python script for fitting a fourier time series with an iterative approach to find the lowest root mean square error. Results for the equation of fit are printed and the series variables returned.

Please modify this for your own use/model if required. See [Licencing](#License) however and pay acknowledgements is all I ask.

## Getting Started

Execute test module to test otherwise just import ffit and punch in the order of the series form you want.

The default setup is a_i * cos(2 pi * i * (x-b_i) ) for an ith order series. This can be changed accordingly to a sine series if required.

Run using ffit.attemptFit(numpy array of x data, numpy array of y data, int n order series you wish to fit, optional:interger of max iterations to attempt per variable)

*Default iterations per variable is 100

*Returns: numpy array of the fit values, float the RMS error, numpy array of the a terms, numpy array of the b terms.

### Prerequisites

Runs with python 3

Additonal python packages required:
>numpy

### Installing

The 'ffit.py' module contains the fitting class to use. Just import into a project for use.

## Authors

* **Daniel Stamer-Squair** - UaineTeine

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
